//
//  PersonalDetailsViewController.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import UIKit

final class PersonalDetailsViewController: BaseViewController {

	let nameLabel = ExtendedLabel.init(title: "Name")
	let surnameLabel = ExtendedLabel.init(title: "Surname")
	let birthDateLabel = ExtendedLabel.init(title: "Birth date")
	let deathDateLabel = ExtendedLabel.init(title: "Death date")
	let burialDateLabel = ExtendedLabel.init(title: "Burial date")
	
	var model: PersonModel!

	init(model: PersonModel){
		self.model = model
		super.init()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.setupLayout()
		self.bindData()
    }
	
	func setupLayout(){
		self.title = "Personal Details"
		self.setupName()
		self.setupSurname()
		self.setupBirthDate()
		self.setupDeathDate()
		self.setupBurialDate()
	}
	
	func setupName(){
		self.view.addSubview(self.nameLabel)
		self.nameLabel.snp.makeConstraints { (make) in
			make.centerX.equalToSuperview()
			make.width.equalToSuperview().multipliedBy(0.9)
			make.top.equalToSuperview().offset(25)
		}
	}
	
	func setupSurname(){
		self.view.addSubview(self.surnameLabel)
		self.surnameLabel.snp.makeConstraints { (make) in
			make.centerX.equalToSuperview()
			make.width.equalTo(self.nameLabel.snp.width)
			make.top.equalTo(self.nameLabel.snp.bottom).offset(20)
		}
	}
	
	func setupBirthDate(){
		self.view.addSubview(self.birthDateLabel)
		self.birthDateLabel.snp.makeConstraints { (make) in
			make.centerX.equalToSuperview()
			make.width.equalTo(self.surnameLabel.snp.width)
			make.top.equalTo(self.surnameLabel.snp.bottom).offset(20)
		}
	}
	
	func setupDeathDate(){
		self.view.addSubview(self.deathDateLabel)
		self.deathDateLabel.snp.makeConstraints { (make) in
			make.centerX.equalToSuperview()
			make.width.equalTo(self.birthDateLabel.snp.width)
			make.top.equalTo(self.birthDateLabel.snp.bottom).offset(20)
		}
	}
	
	func setupBurialDate(){
		self.view.addSubview(self.burialDateLabel)
		self.burialDateLabel.snp.makeConstraints { (make) in
			make.centerX.equalToSuperview()
			make.width.equalTo(self.deathDateLabel.snp.width)
			make.top.equalTo(self.deathDateLabel.snp.bottom).offset(20)
		}
	}
	
	func bindData(){
		guard
			let name = self.model.name,
			let surname = self.model.surname,
			let birthDate = self.model.birthDate,
			let deathDate = self.model.deathDate,
			let burialDate = self.model.burialDate
		else{
			return
		}
		
		self.nameLabel.setValue(value: name.checkEmpty())
		self.surnameLabel.setValue(value: surname.checkEmpty())
		self.birthDateLabel.setValue(value: birthDate.dateToString().checkDate())
		self.deathDateLabel.setValue(value: deathDate.dateToString().checkDate())
		self.burialDateLabel.setValue(value: burialDate.dateToString().checkDate())
	}
}
