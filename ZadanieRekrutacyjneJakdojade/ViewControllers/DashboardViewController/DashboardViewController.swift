//
//  DashboardViewController.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import UIKit
import SnapKit
import KVNProgress

final class DashboardViewController: BaseViewController {

	let tableView = UITableView()
	var data: [GraveModel] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.title = "Dashboard"
		self.setupTableView()
		self.fetchData()
	}
	
	private func setupTableView(){
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.backgroundColor = UIColor.clear
		
		self.view.addSubview(self.tableView)
		self.tableView.snp.makeConstraints { (make) in
			make.edges.equalToSuperview()
		}
	}
	
	private func fetchData(){
		KVNProgress.show()
		Network.GetAllGraves().responseModel(successCompletion: { (responseModel: ResponseModel<GraveModel>) in
			guard
				let models = responseModel.responseArray
				else {
					return
			}
			KVNProgress.showSuccess(completion: {
				self.data = models
				self.tableView.reloadData()
			})
		}, errorCompletion: { (errorMessage) in
			KVNProgress.showError(withStatus: errorMessage)
		})
	}
}

extension DashboardViewController: UITableViewDelegate{
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		guard
			let model = self.data[indexPath.row].person
			else {
				return
		}
		self.navigationController?.pushViewController(PersonalDetailsViewController.init(model: model), animated: true)
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return data.count
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
}

extension DashboardViewController: UITableViewDataSource{
	
	func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
		cell.textLabel?.textColor = UIColor.text
		cell.backgroundColor = UIColor.clear
		cell.selectionStyle = .none
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "cell")
		guard
			let model = self.data[indexPath.row].person
		else {
			return cell
		}
		
		cell.textLabel?.text = model.fullName.checkEmpty()
		return cell
	}
}
