//
//  DateExtensions.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import Foundation

extension Date {
	func dateToString() -> String{
		let formatter = DateFormatter()
		let locale = NSLocale(localeIdentifier: "en_US_POSIX")
		
		formatter.locale = locale as Locale!
		formatter.dateFormat = "dd MMM yyyy"
		
		return formatter.string(from: self)
	}
}
