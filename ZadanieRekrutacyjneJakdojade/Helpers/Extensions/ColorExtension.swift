//
//  ColorExtension.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import UIKit

extension UIColor{
	static let background = UIColor(red:0.09, green:0.09, blue:0.11, alpha:1.00)
	static let navigationBar = UIColor(red:0.22, green:0.24, blue:0.27, alpha:1.00)
	static let text = UIColor(red:0.35, green:0.66, blue:1.00, alpha:1.00)
}
