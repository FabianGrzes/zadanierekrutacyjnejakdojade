//
//  StringExtensions.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import Foundation

extension String{
	func checkEmpty() -> String{
		return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty ? "missing data" : self
	}
	
	func checkDate() -> String{
		return self == "01 Jan 0001" ? "missing data" : self
	}
}
