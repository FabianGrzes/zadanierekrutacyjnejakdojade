//
//  FontExtensions.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import UIKit

extension UIFont{
	static let normal = UIFont.systemFont(ofSize: UIFont.systemFontSize)
	static let small = UIFont.systemFont(ofSize: UIFont.smallSystemFontSize - 2)
}
