//
//  BaseNavigationController.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

	override func viewDidLoad() {
		super.viewDidLoad()
		self.isNavigationBarHidden = false
		self.navigationBar.isTranslucent = false
		self.navigationBar.barTintColor = UIColor.navigationBar
		self.navigationBar.tintColor = UIColor.text
		self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.text]
	}
}
