//
//  CustomDateTransformer.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import ObjectMapper

final class CustomDateTransformer: TransformType {
	internal typealias Object = Date
	internal typealias JSON = String
	
	static var dateFormatter: DateFormatter = {
		let tmp = DateFormatter()
		tmp.dateFormat = "yyyy-MM-dd"
		
		return tmp
	}()
	
	internal func transformFromJSON(_ value: Any?) -> Object? {
		guard let timeString = value as? String else {
			return nil
		}
		return CustomDateTransformer.dateFormatter.date(from: timeString)
	}
	
	internal func transformToJSON(_ value: Date?) -> JSON? {
		guard let date = value else {
			return nil
		}
		
		return CustomDateTransformer.dateFormatter.string(from: date)
	}
}
