//
//  BaseViewController.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
		self.view.backgroundColor = UIColor.background
    }

	override var preferredStatusBarStyle: UIStatusBarStyle {
		return .lightContent
	}
	
	init() {
		super.init(nibName: nil, bundle: nil)
		print("Init: \(self)")
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	deinit {
		print("Deinit: \(self)")
	}
}
