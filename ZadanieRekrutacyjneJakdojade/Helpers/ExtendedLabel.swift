//
//  ExtendedLabel.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import UIKit

class ExtendedLabel: UILabel {
	var title: String
	
	init(title: String){
		self.title = title
		super.init(frame: CGRect.zero)
		self.backgroundColor = UIColor.clear
		self.font = UIFont.normal
		self.textColor = UIColor.text
		self.textAlignment = .left
		self.adjustsFontSizeToFitWidth = true
	}
	
	func setValue(value: String){
		self.text = "\(self.title): \(value)"
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
