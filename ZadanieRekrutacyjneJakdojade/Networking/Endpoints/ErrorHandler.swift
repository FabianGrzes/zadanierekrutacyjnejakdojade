//
//  ErrorHandler.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import Foundation

struct ErrorHandler {
	static func handleError(code: Int) -> String {
		print("Handle error with code: \(code)")
		switch code {
			default:
				return "Something went wrong! Please try again."
		}
	}
}
