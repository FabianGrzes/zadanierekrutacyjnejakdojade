//
//  Graves.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

enum Network {
	case GetAllGraves()
	
	private var method: HTTPMethod{
		
		switch self {
			case .GetAllGraves():
				return .get
		}
	}
	
	var url: String {
		return "http://www.poznan.pl/featureserver/featureserver.cgi/" + self.urlEnd
		
	}
	
	private var headers: HTTPHeaders{
		return [:]
	}
	
	private var urlEnd: String {
		switch self {
		case .GetAllGraves():
			return "groby/"
		}
	}
	
	private var parameters: Parameters {
		switch self {
			case .GetAllGraves():
			return [
				"maxFeatures" : 3000
			]
		}
	}
	
	func responseModel<T:Mappable>(successCompletion: @escaping (T) -> Void, errorCompletion: ((String) -> Void)?) {
		print("***START***\nRequest for Model: \nurl: \(self.url)\nmethod: \(self.method)\nparameters: \(self.parameters)\n***END***")
		
		Alamofire
			.request(
				self.url,
				method: self.method,
				parameters: self.parameters,
				headers: self.headers)
			.validate()
			.responseObject {
				(response: DataResponse<T>) in
				switch response.result {
				case .success:
					guard let model = response.result.value else{
						guard let function = errorCompletion
							else{
								return
						}
						function(ErrorHandler.handleError(code: 900))
						return
					}
					
					successCompletion(model)
					
				case .failure(let error):
					guard let function = errorCompletion
						else{
							return
					}
					function(ErrorHandler.handleError(code: (error as? AFError)?.responseCode ?? 0))
				}
		}
	}
}
