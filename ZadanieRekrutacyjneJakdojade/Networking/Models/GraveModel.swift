//
//  GraveModel.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import ObjectMapper

class GraveModel: Mappable {
	var id: Int?
	var person: PersonModel?
	
	required init?(map: Map) {
		if map.JSON["id"] == nil {
			return nil
		}
	}
	
	func mapping(map: Map) {
		self.id <- map["id"]
		self.person <- map["properties"]
	}
}
