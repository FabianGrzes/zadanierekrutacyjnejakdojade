//
//  ResponseModel.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import ObjectMapper

class ResponseModel<T: Mappable>: Mappable {
	var responseArray: [T]?
	
	required init?(map: Map) {}
	
	func mapping(map: Map) {
		self.responseArray <- map["features"]
	}
}
