//
//  PersonModel.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import ObjectMapper

class PersonModel: Mappable {
	var name: String?
	var surname: String?
	var birthDate: Date?
	var deathDate: Date?
	var burialDate: Date?
	
	var fullName: String! {
		get {
			return "\(self.surname ?? "") \(self.name ?? "")"
		}
	}

	required init?(map: Map) {
	}

	func mapping(map: Map) {
		self.name <- map["print_name"]
		self.surname <- map["print_surname"]
		self.birthDate <- (map["g_date_birth"], CustomDateTransformer())
		self.deathDate <- (map["g_date_death"], CustomDateTransformer())
		self.burialDate <- (map["g_date_burial"], CustomDateTransformer())
	}
}

