//
//  Initializers.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import UIKit
import KVNProgress

struct Initializers {
	private init(){}
	
	static func initializeAll(){
		self.initializeKVNProgress()
	}
	
	private static func initializeKVNProgress(){
		let config = KVNProgressConfiguration.init()
		config.backgroundTintColor = UIColor.background
		config.circleStrokeForegroundColor = UIColor.text
		config.successColor = UIColor.text
		config.statusColor = UIColor.text
		config.errorColor = UIColor.text
		config.statusFont = UIFont.small
		config.isFullScreen = true
		KVNProgress.setConfiguration(config)
	}
}
