//
//  FlowManager.swift
//  ZadanieRekrutacyjneJakdojade
//
//  Created by Fabian Grześ on 15.05.2017.
//  Copyright © 2017 Fabian Grześ. All rights reserved.
//

import Foundation
import UIKit

struct FlowManager {
	private init(){}
	
	static func loadDashboard(){
		guard let appDelegate = UIApplication.shared.delegate as? AppDelegate
			else {
				fatalError()
		}
		
		let navigationController = BaseNavigationController.init(rootViewController: DashboardViewController())
		
		appDelegate.window?.rootViewController = navigationController
		appDelegate.window?.makeKeyAndVisible()
	}
}
